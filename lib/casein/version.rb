module Casein
  VERSION_HASH = { major: 1, minor: 3, patch: 3, build: 7 }
  VERSION = VERSION_HASH.values.join(".")
end
